# Welcome to ASP.NET Core with Vue.js


## This application consists of:

*   Sample pages using ASP.NET Core and Vue.js
*   [NPM](https://www.npmjs.com) for managing client-side libraries
*   Theming using [Bootstrap v4](https://v4-alpha.getbootstrap.com)

*	Webpack dev middleware. In development mode, there's no need to run the webpack build tool. Your client-side resources are dynamically built on demand. Updates are available as soon as you modify any file.
*	Hot module replacement. In development mode, you don't even need to reload the page after making most changes. Within seconds of saving changes to files, your app will be rebuilt and a new instance is injected into the page.
*	Efficient production builds. In production mode, development-time features are disabled, and the webpack build tool produces minified static CSS and JavaScript files.


## Prerequiste
1.	Install [Node.js and npm](https://nodejs.org/en/download/) if they are not already on your machine.
2.	Install [.NET Core 1.1](https://www.microsoft.com/net/download/core#/current).
3.	The minimum requirement is Visual Studio Update 3

## Run & Deploy
1.	Open the project
2.	Go to Package manager and enter ```npm install```. This will install required libraries defined in package.json
3.	Hit F5 and wait for the browser to start

### Run with node.js
If you only want to debug errors on the UI side and does not need any API calls it's faster to start from node.js:
1. Start cmd
2. Go to dir
3. ```npm run dev``` 

> This will also give better hints when you have Lint errors or other build errors in JavaScript

## Description
The project has the following setup:

### ClientApp/App.vue
The root module to configure Vue

### ClientApp/components
All components are kept here. The .vue file is the main file for the component. This file keeps the template, scripts and styling that should be special for this component only.

### ClientApp/layouts
Each page will be kept here.

### ClientApp/layouts/devices
Devices shows how it's possible to use a JavaScript class that loads data with JQuery.

### ClientApp/components/menu.component.vue
This is the menu

### ClientApp/routes.js

## Modifications
>	Update this section if there are special modifications that needs to be done when adding new components


## Resources
*	Vue.js Documentation: https://vuejs.org/v2/guide

### Lint
*	Configure ESLint http://eslint.org/docs/2.0.0/user-guide/configuring

## Other URL:s of interests
*	https://github.com/prograhammer/example-vue-project/blob/master/src/main.js#L2

# Todo
## Include jQuery with webpack
At the moment I'm hijacking the page by including the jQuery from a CDN in index.html. Remove this one and fix with webpack instead
*	https://laracasts.com/discuss/channels/vue/include-jquery-in-webpack-vuejs-l-53
*	https://github.com/webpack/webpack/issues/2800
*	https://laracasts.com/discuss/channels/vue/jquery-and-vuejs
*	https://forum-archive.vuejs.org/topic/882/newb-how-to-use-jquery-with-vue/7

## Include Bootstrap with webpack
Same as above

# vuewebappcore

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



# Essential Links
*	Core Docs: https://vuejs.org
* 	Forum: https://forum.vuejs.org
* 	Gitter Chat: https://gitter.im/vuejs/vue
*	Twitter: https://twitter.com/vuejs

## Ecosystem
*	https://router.vuejs.org/en/
*	https://vuex.vuejs.org/en/intro.html
*	https://vue-loader.vuejs.org/en/