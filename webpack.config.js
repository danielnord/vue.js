﻿"use strict";

var isDevBuild = process.argv.indexOf('--env.prod') < 0;
var devConfig = require("./webpack.config.development");
var prodConfig = require("./webpack.config.production");

module.exports = isDevBuild ? devConfig : prodConfig;