﻿export default class ApiDevice {
    list(callback) {
        var json = {};

        $.get('/api/devices').done(
            function(data) {
                callback(data);
            }
        );
        return true;
    }
}
