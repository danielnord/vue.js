
import Home from './layouts/home'
import Devices from './layouts/devices'
import NotFound from './layouts/notFound'

const main = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/devices',
    name: 'devices',
    component: Devices
  },
  {
    path: '/home',
    redirect: '/'
  }
]

const error = [
  {
    path: '*',
    name: 'error',
    component: NotFound
  }
]

export default [].concat(main, error)
