/* Vue */
import Vue from 'vue'
import VueRouter from 'vue-router'

// Routes
import routes from './routes'

// plugins
Vue.use(VueRouter);

// router config
let router = new VueRouter({
  routes
});
router.afterEach((currentRoute) => {
  let mainContent = document.querySelector('.main-content');
    if (mainContent) {
    mainContent.scrollTop = 0;
    }
});
import App from './App'

let VueApp = Vue.component('app', App);

/* eslint-disable no-unused-vars */
const app = new VueApp({
  el: '#app',
  router
});
