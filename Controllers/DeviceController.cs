using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace VueWebAppCore.Controllers
{
    public class DeviceController : Controller
    {
        [HttpGet]
        [Route("api/devices")]
        public IEnumerable<Device> GetDevices()
        {
            var result = new List<Device>();
            result.Add(new Device { name = "TEST00001", status = 1 });
            result.Add(new Device { name = "TEST00002", status = 2 });
            result.Add(new Device { name = "TEST00003", status = 0 });
            result.Add(new Device { name = "TEST00004", status = 1 });
            result.Add(new Device { name = "TEST00005", status = 1 });
            result.Add(new Device { name = "TEST00006", status = 1 });
            result.Add(new Device { name = "TEST00007", status = 1 });
            result.Add(new Device { name = "TEST00008", status = 1 });
            result.Add(new Device { name = "TEST00009", status = 1 });
            result.Add(new Device { name = "TEST00010", status = 1 });
            result.Add(new Device { name = "TEST00011", status = 2 });
            result.Add(new Device { name = "TEST00012", status = 1 });
            result.Add(new Device { name = "TEST00013", status = 2 });
            result.Add(new Device { name = "TEST00014", status = 1 });
            result.Add(new Device { name = "TEST00015", status = 0 });
            result.Add(new Device { name = "TEST00016", status = 1 });
            result.Add(new Device { name = "TEST00017", status = 1 });
            result.Add(new Device { name = "TEST00018", status = 2 });
            result.Add(new Device { name = "TEST00019", status = 1 });
            result.Add(new Device { name = "TEST00020", status = 1 });


            return result;
        }

        public class Device
        {
            public int status { get; set; }
            public string name { get; set; }
        }
    }
}
